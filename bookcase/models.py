from django.conf import settings
from django.db import models
from django.utils import timezone


class Book(models.Model):
    id = models.Count
    author = models.CharField(max_length=100)
    title = models.CharField(max_length=100)

    def get_author(self):
        return self.author

    def get_title(self):
        return self.title
