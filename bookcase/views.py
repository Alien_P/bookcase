from django.shortcuts import render
from .models import Book


def book_list(request):
    books = Book.objects.order_by('author')
    return render(request, 'bookcase/book_list.html', {'books': books})
